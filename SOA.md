# Service-oriented architecture (SOA)

Service-oriented architecture (SOA) is a style of software design where services are provided to the other components by application components, through a communication protocol over a network. A SOA service is a discrete unit of functionality that can be accessed remotely and acted upon and updated independently, such as retrieving a credit card statement online. SOA is also intended to be independent of vendors, products and technologies.

Refer link to known more about it           &nbsp;      [SOA-wikipedia](https://en.wikipedia.org/wiki/Service-oriented_architecture)

### What is Service?
A service is a well-defined, self-contained function that represents a unit of functionality. A service can exchange information from another service. It is not dependent on the state of another service. It uses a loosely coupled, message-based communication model to communicate with applications and other services.

### Service Connections
Service consumer sends a service request to the service provider, and the service provider sends the service response to the service consumer. The service connection is understandable to both the service consumer and service provider.
 
### Service-Oriented Terminologies
* **Services** - The services are the logical entities defined by one or more published interfaces
* **Service provider** - It is a software entity that implements a service specification.
* **Service consumer** - It can be called as a requestor or client that calls a service provider. A service consumer can be another service or an end-user application.
* **Service locator** - It is a service provider that acts as a registry. It is responsible for examining service provider interfaces and service locations.
Service broker - It is a service provider that pass service requests to one or more additional service providers.
 
 ### Elements Of SOA

 ![elements](https://www.w3schools.in/wp-content/uploads/2016/06/SOA.jpg)

 ### Advantages of SOA
 * Loose coupling between services
 * Maintenance is easy 
 * Quality of code improved
 * Scalable 


[Referred link](https://www.w3schools.in/service-oriented-architecture/)